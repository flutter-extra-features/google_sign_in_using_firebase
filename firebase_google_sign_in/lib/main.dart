import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:firebase_google_sign_in/firebase_options.dart';
import 'package:firebase_google_sign_in/pages/home_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HomePage(),
    );
  }
}

/*
  Packages Used:-
    1] flutter pub add firebase_auth
    2] flutter pub add firebase_core
    3] flutter pub add sign_in_button

  Changed minSdkVersion in build.gradle

  Used Firebase :-

  1] Create project in firebase
  2] firebase login in vscode
  3] flutterfire configure
  
  If there is an error such as hash

  1] Go to build\app\outputs\apk\debug> 
  2] Open in terminal above folder
  3] type  keytool -printcert -jarfile app-debug.apk
  4] copy  Certificate fingerprint SHA1: DB:6D:67:4F:E4:1C:81:2F:9F:A8:E2:DA:52:F8:BB:05:0E:19:05:42 
  5] Paste in used Firebase project under SHA certificate fingerprint under general project settings
 */